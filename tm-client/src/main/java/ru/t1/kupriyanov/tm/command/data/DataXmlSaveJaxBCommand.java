package ru.t1.kupriyanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.DataXmlLoadJaxBRequest;
import ru.t1.kupriyanov.tm.dto.request.DataXmlSaveJaxBRequest;
import ru.t1.kupriyanov.tm.enumerated.Role;

public final class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[SAVE XML DATA]");
        getDomainEndpoint().saveDataXmlJaxB(new DataXmlSaveJaxBRequest(getToken()));
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to an .xml file.";
    }

    @Override
    @NotNull
    public  String getName() {
        return "save-xml-data";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
