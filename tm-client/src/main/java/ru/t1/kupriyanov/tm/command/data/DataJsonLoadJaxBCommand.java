package ru.t1.kupriyanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.DataJsonLoadJaxBRequest;
import ru.t1.kupriyanov.tm.enumerated.Role;

public final class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[LOAD JSON DATA]");
        getDomainEndpoint().loadDataJsonJaxB(new DataJsonLoadJaxBRequest(getToken()));
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from a .json file.";
    }

    @Override
    @NotNull
    public  String getName() {
        return "load-json-data";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
