package ru.t1.kupriyanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.DataXmlLoadJaxBRequest;
import ru.t1.kupriyanov.tm.enumerated.Role;

public final class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[LOAD XML DATA]");
        getDomainEndpoint().loadDataXmlJaxB(new DataXmlLoadJaxBRequest(getToken()));
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from an .xml file.";
    }

    @Override
    @NotNull
    public  String getName() {
        return "load-xlm-data";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
