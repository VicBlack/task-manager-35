package ru.t1.kupriyanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.UserUpdateProfileRequest;
import ru.t1.kupriyanov.tm.enumerated.Role;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "update-user-profile";

    @NotNull
    private final String DESCRIPTION = "update user's profile";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER'S PROFILE]");
        System.out.println("FIRST NAME: ");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME: ");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME: ");
        @Nullable final String middleName = TerminalUtil.nextLine();
        getUserEndpoint().updateUserProfile(new UserUpdateProfileRequest(getToken(), firstName, lastName, middleName));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
