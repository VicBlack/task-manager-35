package ru.t1.kupriyanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.ProjectRemoveByIndexRequest;
import ru.t1.kupriyanov.tm.dto.response.ProjectRemoveByIndexResponse;
import ru.t1.kupriyanov.tm.model.Project;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectRemoveByIndexRequest request =
                new ProjectRemoveByIndexRequest(getToken(), index);
        @NotNull final ProjectRemoveByIndexResponse response = getProjectEndpoint().removeProjectByIndex(request);
        @Nullable final Project project = response.getProject();
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by index.";
    }

}
