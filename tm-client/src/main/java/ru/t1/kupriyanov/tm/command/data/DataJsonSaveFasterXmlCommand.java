package ru.t1.kupriyanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.DataJsonSaveFasterXmlRequest;
import ru.t1.kupriyanov.tm.enumerated.Role;

public final class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[SAVE JSON DATA FASTER]");
        getDomainEndpoint().saveDataJsonFasterXml(new DataJsonSaveFasterXmlRequest(getToken()));
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to a .json file faster.";
    }

    @Override
    @NotNull
    public  String getName() {
        return "save-data-json-faster";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
