package ru.t1.kupriyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.t1.kupriyanov.tm.api.repository.IProjectRepository;
import ru.t1.kupriyanov.tm.api.repository.ITaskRepository;
import ru.t1.kupriyanov.tm.api.repository.IUserRepository;
import ru.t1.kupriyanov.tm.api.service.IPropertyService;
import ru.t1.kupriyanov.tm.api.service.IUserService;
import ru.t1.kupriyanov.tm.enumerated.Role;
import ru.t1.kupriyanov.tm.exception.field.IdEmptyException;
import ru.t1.kupriyanov.tm.exception.user.*;
import ru.t1.kupriyanov.tm.model.User;
import ru.t1.kupriyanov.tm.repository.ProjectRepository;
import ru.t1.kupriyanov.tm.repository.TaskRepository;
import ru.t1.kupriyanov.tm.repository.UserRepository;

import java.util.Random;
import java.util.UUID;

public class UserServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(
            propertyService,
            userRepository,
            taskRepository,
            projectRepository
    );

    @NotNull
    private final String login = UUID.randomUUID().toString();

    @NotNull
    private final String password = UUID.randomUUID().toString();

    @NotNull
    private final String email = UUID.randomUUID().toString();

    @NotNull
    private final Random random = new Random();

    @Test
    public void create() {
        @Nullable final String emptyEmail = null;
        @Nullable final Role emptyRole = null;
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", ""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(login, null));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.create(login, password, emptyEmail));
        Assert.assertThrows(RoleEmptyException.class, () -> userService.create(login, password, emptyRole));
        Assert.assertEquals(0, userService.getSize());
        final int repositorySize = 3;
        for (int i = 0; i < repositorySize; i++) {
            Assert.assertEquals(i, userService.getSize());
            userService.create(String.format("User_LoginNumber_%d", i), password);
        }
        Assert.assertEquals(repositorySize, userService.getSize());
    }

    @Test
    public void findByLogin() {
        Assert.assertEquals(0, userService.getSize());
        @Nullable User user = userService.create(login, password);
        Assert.assertEquals(1, userService.getSize());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
        Assert.assertEquals(user, userService.findByLogin(login));

    }

    @Test
    public void findByEmail() {
        Assert.assertEquals(0, userService.getSize());
        @Nullable User user = userService.create(login, password, email);
        Assert.assertEquals(1, userService.getSize());
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(null));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(""));
        Assert.assertEquals(user, userService.findByEmail(email));

    }

    @Test
    public void removeByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(""));
        final int repositorySize = 4;
        for (int i = 0; i < repositorySize; i++) {
            Assert.assertEquals(i, userService.getSize());
            userService.create(String.format("User_LoginNumber_%d", i), password, String.format("User_Email_%d", i));
        }
        Assert.assertEquals(repositorySize, userService.getSize());
        final int randInt = random.nextInt(4);
        @NotNull final String login = "User_LoginNumber_" + randInt;
        userService.removeByLogin(login);
        Assert.assertEquals(repositorySize - 1, userService.getSize());
        Assert.assertFalse(userService.isLoginExist(login));
    }

    @Test
    public void removeByEmail() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(null));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(""));
         final int repositorySize = 4;
        for (int i = 0; i < repositorySize; i++) {
            Assert.assertEquals(i, userService.getSize());
            userService.create(String.format("User_LoginNumber_%d", i), password, String.format("User_Email_%d", i));
        }
        Assert.assertEquals(repositorySize, userService.getSize());
         final int randInt = random.nextInt(4);
        @NotNull final String email = "User_Email_" + randInt;
        userService.removeByEmail(email);
        Assert.assertEquals(repositorySize - 1, userService.getSize());
        Assert.assertFalse(userService.isEmailExist(email));
    }

    @Test
    public void setPassword() {
        @Nullable User user = userService.create(login, password);
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword(null, password));
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword("", password));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(user.getId(), null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(user.getId(),""));
        Assert.assertEquals(user.getPasswordHash(), userService.findByLogin(login).getPasswordHash());
    }

    @Test
    public void updateUser() {
        @NotNull String newId = UUID.randomUUID().toString();
        @NotNull String updateFirstName = UUID.randomUUID().toString();
        @NotNull String updateLastName = UUID.randomUUID().toString();
        @NotNull String updateMiddleName = UUID.randomUUID().toString();
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> userService.updateUser(newId, updateFirstName, updateLastName, updateMiddleName)
        );
        @Nullable User user = userService.create(login, password);
        @Nullable User updatedUser = userService.updateUser(user.getId(), updateFirstName, updateLastName, updateMiddleName);
        Assert.assertEquals(user, updatedUser);
        Assert.assertEquals(updateFirstName, updatedUser.getFirstName());
        Assert.assertEquals(updateLastName, updatedUser.getLastName());
        Assert.assertEquals(updateMiddleName, updatedUser.getMiddleName());
    }

    @Test
    public void isLoginExist() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.isLoginExist(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.isLoginExist(""));
        final int repositorySize = 4;
        for (int i = 0; i < repositorySize; i++) {
            Assert.assertEquals(i, userService.getSize());
            userService.create(String.format("User_LoginNumber_%d", i), password);
        }
        final int randInt = random.nextInt(4);
        @NotNull final String login = "User_LoginNumber_" + randInt;
        Assert.assertTrue(userService.isLoginExist(login));
    }

    @Test
    public void isEmailExist() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.isEmailExist(null));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.isEmailExist(""));
        final int repositorySize = 4;
        for (int i = 0; i < repositorySize; i++) {
            Assert.assertEquals(i, userService.getSize());
            userService.create(String.format("User_LoginNumber_%d", i), password, String.format("User_Email_%d", i));
        }
        final int randInt = random.nextInt(4);
        @NotNull final String userMail = "User_Email_" + randInt;
        Assert.assertTrue(userService.isEmailExist(userMail));
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(""));
        @Nullable User user = userService.create(login, password);
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin(login);
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(""));
        @Nullable User user = userService.create(login, password);
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin(login);
        Assert.assertTrue(user.getLocked());
        userService.unlockUserByLogin(login);
        Assert.assertFalse(user.getLocked());
    }

}