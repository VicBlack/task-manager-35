package ru.t1.kupriyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.t1.kupriyanov.tm.api.repository.ITaskRepository;
import ru.t1.kupriyanov.tm.api.service.ITaskService;
import ru.t1.kupriyanov.tm.enumerated.Status;
import ru.t1.kupriyanov.tm.enumerated.TaskSort;
import ru.t1.kupriyanov.tm.exception.field.*;
import ru.t1.kupriyanov.tm.model.Task;
import ru.t1.kupriyanov.tm.repository.TaskRepository;

import java.util.List;
import java.util.UUID;

public class TaskServiceTest {

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final ITaskService service = new TaskService(repository);

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String name = UUID.randomUUID().toString();

    @NotNull
    private final String description = UUID.randomUUID().toString();

    @NotNull
    private final Status status = Status.COMPLETED;

    @Test
    public void createTask() {
        Assert.assertEquals(0, service.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(null, name, description));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null, description));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(userId, name, null));
        @NotNull final Task task = service.create(userId, name, description);
        Assert.assertEquals(1, service.getSize(userId));
    }

    @Test
    public void updateById() {
        @NotNull final String updateDesription = UUID.randomUUID().toString();
        @NotNull final String updateName = UUID.randomUUID().toString();
        @NotNull final Task task = service.create(userId, name, description);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateById(null, task.getId(), updateName, updateDesription));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateById(userId, task.getId(), null, updateDesription));
        service.updateById(userId, task.getId(), updateName, updateDesription);
        Assert.assertEquals(updateName, service.findOneById(userId, task.getId()).getName());
        Assert.assertEquals(updateDesription, service.findOneById(userId, task.getId()).getDescription());
    }

    @Test
    public void updateByIndex() {
        @NotNull final String updateDesription = UUID.randomUUID().toString();
        @NotNull final String updateName = UUID.randomUUID().toString();
        service.create(userId, name, description);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateByIndex(null, 0, updateName, updateDesription));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.updateByIndex(userId, null, null, updateDesription));
        service.updateByIndex(userId, 0, updateName, updateDesription);
        Assert.assertEquals(updateName, service.findOneByIndex(userId, 0).getName());
        Assert.assertEquals(updateDesription, service.findOneByIndex(userId, 0).getDescription());
    }

    @Test
    public void changeTaskStatusByIndex() {
        service.create(userId, name, description);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeTaskStatusByIndex(null, 0, status));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.changeTaskStatusByIndex(userId, 1, status));
        Assert.assertThrows(StatusEmptyException.class, () -> service.changeTaskStatusByIndex(userId, 0, null));
        service.changeTaskStatusByIndex(userId, 0, status);
        Assert.assertEquals(service.findOneByIndex(userId, 0).getStatus(), status);
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final Task task = service.create(userId, name, description);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeTaskStatusById(null, task.getId(), status));
        Assert.assertThrows(IdEmptyException.class, () -> service.changeTaskStatusById(userId, null, status));
        Assert.assertThrows(StatusEmptyException.class, () -> service.changeTaskStatusById(userId, task.getId(), null));
        service.changeTaskStatusById(userId, task.getId(), status);
        Assert.assertEquals(service.findOneById(userId, task.getId()).getStatus(), status);
    }


    @Test
    public void removeAll() {
        @Nullable final String emptyUser = null;
        Assert.assertEquals(0, service.getSize());
        service.create(userId, UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Assert.assertEquals(1, service.getSize());
        service.create(userId, UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Assert.assertEquals(2, service.getSize());
        service.removeAll();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void existsById() {
        @NotNull final Task task = service.create(userId, name, description);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById(null, task.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, null));
        Assert.assertTrue(service.existsById(task.getId()));
    }

    @Test
    public void findAll() {
        repository.create(userId, name, description);
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId,null));
        Assert.assertEquals(1, service.getSize());
        repository.create(userId, name, description);
        Assert.assertEquals(2, service.getSize());
        repository.create(userId, name, description);
        Assert.assertEquals(3, service.getSize());
        repository.create(userId, name, description);
        Assert.assertEquals(4, service.getSize());
    }

    @Test
    public void findAllByComparator() {
        final int repositorySize = 4;
        for (int i = 0; i < repositorySize; i++) {
            @NotNull final String name = String.format("Test_Task#_%d", repositorySize - i - 1);
            service.create(userId, name, description);
        }
        @NotNull List<Task> tasks = service.findAll(userId, TaskSort.BY_NAME.getComparator());
        Assert.assertEquals(repositorySize, tasks.size());
        for (int i = 0; i < repositorySize; i++) {
            @NotNull final String name = String.format("Test_Task#_%d", i);
            Assert.assertEquals(name, tasks.get(i).getName());
        }
    }

    @Test
    public void findOneById() {
        Assert.assertEquals(0, service.getSize());
        @NotNull final Task task = service.create(userId, name, description);
        Assert.assertNotNull(service.findOneById(task.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById(null, task.getId()));
        Assert.assertEquals(task.getId(), service.findOneById(userId, task.getId()).getId());
    }

    @Test
    public void findOneByIndex() {
        @NotNull final Task task = service.create(userId, name, description);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex(null,0));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(userId, 1));
        Assert.assertEquals(task.getId(), service.findOneByIndex(userId, 0).getId());
    }

    @Test
    public void removeOneById() {
        Assert.assertEquals(0, service.getSize());
        @NotNull final Task task = service.create(userId, name, description);
        Assert.assertEquals(1, service.getSize());
        Assert.assertThrows(IdEmptyException.class, () -> service.removeOneById(null));
        service.removeOneById(task.getId());
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void removeOneByIndex() {
        Assert.assertEquals(0, service.getSize());
        @NotNull final Task task = service.create(userId, name, description);
        Assert.assertEquals(1, service.getSize());
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeOneByIndex( 1));
        service.removeOneByIndex(0);
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void add() {
        Assert.assertEquals(0, service.getSize());
        @NotNull final Task task = new Task();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(null, task));
        @Nullable Task updateTask = service.add(userId, task);
        Assert.assertNotNull(updateTask);
        Assert.assertEquals(task, updateTask);
        Assert.assertEquals(1, service.getSize());
    }

    @Test
    public void removeOne() {
        final int repositorySize = 4;
        for (int i = 0; i < repositorySize; i++) {
            @NotNull final String taskNameRandom = UUID.randomUUID().toString();
            @NotNull final String taskDescriptionRandom = UUID.randomUUID().toString();
            service.create(userId, taskNameRandom, taskDescriptionRandom);
        }
        Assert.assertEquals(repositorySize, service.getSize(userId));
        @NotNull final Task task = service.removeOne(service.findOneByIndex(userId, 0));
        Assert.assertEquals(repositorySize - 1, service.getSize(userId));
        Assert.assertFalse(service.existsById(task.getId()));
    }

    @Test
    public void getSize() {
        final int repositorySize = 4;
        for (int i = 0; i < repositorySize; i++) {
            @NotNull final String userIdRandom = UUID.randomUUID().toString();
            @NotNull final String taskNameRandom = UUID.randomUUID().toString();
            @NotNull final String taskDescriptionRandom = UUID.randomUUID().toString();
            service.create(userIdRandom, taskNameRandom, taskDescriptionRandom);
        }
        @NotNull final List<Task> tasks = service.findAll();
        Assert.assertEquals(repositorySize, tasks.size());
    }

}
