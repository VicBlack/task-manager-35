package ru.t1.kupriyanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.t1.kupriyanov.tm.api.repository.ITaskRepository;
import ru.t1.kupriyanov.tm.enumerated.TaskSort;
import ru.t1.kupriyanov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String name = UUID.randomUUID().toString();

    @NotNull
    private final String description = UUID.randomUUID().toString();

    @Test
    public void create() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, name, description);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), task.getId()));
        @Nullable final Task taskFind = repository.findOneById(userId, task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task, taskFind);
        Assert.assertEquals(task.getDescription(), taskFind.getDescription());
    }

    @Test
    public void add() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        repository.add(task);
        Assert.assertEquals(1, repository.getSize());
    }

    @Test
    public void clear() {
        Assert.assertEquals(0, repository.getSize());
        repository.create(userId, name, description);
        Assert.assertEquals(1, repository.getSize());
        repository.removeAll();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void findAll() {
        repository.create(userId, name, description);
        Assert.assertEquals(1, repository.findAll().size());
        repository.create(userId, name, description);
        Assert.assertEquals(2, repository.findAll().size());
        repository.create(userId, name, description);
        Assert.assertEquals(3, repository.findAll().size());
        repository.create(userId, name, description);
        Assert.assertEquals(4, repository.findAll().size());
    }

    @Test
    public void findAllUserId() {
        repository.create(userId, name, description);
        Assert.assertEquals(0, repository.findAll(UUID.randomUUID().toString()).size());
        Assert.assertEquals(1, repository.findAll(userId).size());
    }

    @Test
    public void findAllComparator() {
        final int repositorySize = 4;
        for (int i = 0; i < repositorySize; i++) {
            @NotNull final String name = String.format("Project_%d", repositorySize - i - 1);
            repository.create(userId, name, description);
        }
        @NotNull List<Task> tasks = repository.findAll(userId, TaskSort.BY_NAME.getComparator());
        Assert.assertEquals(repositorySize, tasks.size());
        for (int i = 0; i < repositorySize; i++) {
            @NotNull final String name = String.format("Project_%d", i);
            Assert.assertEquals(name, tasks.get(i).getName());
        }
    }

    @Test
    public void existsByIdUserCreator() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, name, description);
        Assert.assertTrue(repository.existsById(task.getId()));
    }

    @Test
    public void existsByIdUserRandom() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, name, description);
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString(), task.getId()));
    }

    @Test
    public void existsByIdUserEmpty() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, name, description);
        Assert.assertFalse(repository.existsById("", task.getId()));
    }

    @Test
    public void findOneByIdUserCreator() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, name, description);
        Assert.assertNotNull(repository.findOneById(task.getId()));
        @Nullable final Task taskFind = repository.findOneById(userId, task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task.getId(), taskFind.getId());
    }

    @Test
    public void findOneByIdUserRandom() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, name, description);
        Assert.assertNotNull(repository.findOneById(task.getId()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), task.getId()));
        Assert.assertNull(repository.findOneById(userId, UUID.randomUUID().toString()));
    }

    @Test
    public void findOneByIdUserEmpty() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, name, description);
        Assert.assertNotNull(repository.findOneById(task.getId()));
        Assert.assertNull(repository.findOneById(null, task.getId()));
        Assert.assertNull(repository.findOneById("", task.getId()));
    }

    @Test
    public void findOneByIndexUserCreator() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, name, description);
        Assert.assertNotNull(repository.findOneByIndex(0));
        Assert.assertEquals(task.getId(), repository.findOneByIndex(0).getId());
        Assert.assertNotNull(repository.findOneByIndex(userId, 0));
    }

    @Test
    public void findOneByIndexUserRandom() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, name, description);
        Assert.assertNotNull(repository.findOneByIndex(0));
        Assert.assertEquals(task.getId(), repository.findOneByIndex(0).getId());
        Assert.assertNull(repository.findOneByIndex(UUID.randomUUID().toString(), 0));
    }

    @Test
    public void findOneByIndexUserEmpty() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, name, description);
        Assert.assertNotNull(repository.findOneByIndex(0));
        Assert.assertEquals(task.getId(), repository.findOneByIndex(0).getId());
        Assert.assertNull(repository.findOneByIndex("", 0));
    }

    @Test
    public void remove() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, name, description);
        @Nullable final Task taskRemove = repository.removeOne(task);
        Assert.assertNotNull(taskRemove);
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void removeAll() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull List<Task> tasks = new ArrayList<>();
        final int repositorySize = 4;
        for (int i = 0; i < repositorySize; i++) {
            @NotNull final String userId = UUID.randomUUID().toString();
            @NotNull final String projectName = UUID.randomUUID().toString();
            @NotNull final Task task = new Task();
            task.setName(projectName);
            task.setUserId(userId);
            tasks.add(task);
        }
        repository.add(tasks);
        Assert.assertEquals(repositorySize, repository.getSize());
        repository.removeAll();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void removeById() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, name, description);
        Assert.assertEquals(1, repository.getSize());
        repository.removeOneById(UUID.randomUUID().toString());
        Assert.assertEquals(1, repository.getSize());
        repository.removeOneById(task.getId());
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void removeByIndex() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, name, description);
        @Nullable final Task taskRemove = repository.removeOneByIndex(0);
        Assert.assertNotNull(taskRemove);
        Assert.assertEquals(0, repository.getSize());
        Assert.assertEquals(task, taskRemove);
    }

    @Test
    public void count() {
        Assert.assertEquals(0, repository.getSize());
        final int repositorySize = 4;
        for (int i = 0; i < repositorySize; i++) {
            @NotNull final String userId = UUID.randomUUID().toString();
            @NotNull final String name = UUID.randomUUID().toString();
            repository.create(userId, name, description);
            Assert.assertEquals(i + 1, repository.getSize());
            Assert.assertEquals(repository.getSize(), repository.findAll().size());
        }
    }

}
