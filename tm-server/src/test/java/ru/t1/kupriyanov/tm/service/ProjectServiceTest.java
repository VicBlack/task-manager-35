package ru.t1.kupriyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.t1.kupriyanov.tm.api.repository.IProjectRepository;
import ru.t1.kupriyanov.tm.api.service.IProjectService;
import ru.t1.kupriyanov.tm.enumerated.ProjectSort;
import ru.t1.kupriyanov.tm.enumerated.Status;
import ru.t1.kupriyanov.tm.exception.field.*;
import ru.t1.kupriyanov.tm.model.Project;
import ru.t1.kupriyanov.tm.repository.ProjectRepository;

import java.util.List;
import java.util.UUID;

public class ProjectServiceTest {

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @NotNull
    private final IProjectService service = new ProjectService(repository);

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String name = UUID.randomUUID().toString();

    @NotNull
    private final String description = UUID.randomUUID().toString();

    @NotNull
    private final Status status = Status.COMPLETED;

    @Test
    public void createProject() {
        Assert.assertEquals(0, service.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(null, name, description));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null, description));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(userId, name, null));
        @NotNull final Project project = service.create(userId, name, description);
        Assert.assertEquals(1, service.getSize(userId));
    }

    @Test
    public void updateById() {
        @NotNull final String updateDescription = UUID.randomUUID().toString();
        @NotNull final String updateName = UUID.randomUUID().toString();
        @NotNull final Project project = service.create(userId, name, description);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateById(null, project.getId(), updateName, updateDescription));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateById(userId, project.getId(), null, updateDescription));
        service.updateById(userId, project.getId(), updateName, updateDescription);
        Assert.assertEquals(updateName, service.findOneById(userId, project.getId()).getName());
        Assert.assertEquals(updateDescription, service.findOneById(userId, project.getId()).getDescription());
    }

    @Test
    public void updateByIndex() {
        @NotNull final String updateDescription = UUID.randomUUID().toString();
        @NotNull final String updateName = UUID.randomUUID().toString();
        service.create(userId, name, description);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateByIndex(null, 0, updateName, updateDescription));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateByIndex(userId, 0, null, updateDescription));
        service.updateByIndex(userId, 0, updateName, updateDescription);
        Assert.assertEquals(updateName, service.findOneByIndex(userId, 0).getName());
        Assert.assertEquals(updateDescription, service.findOneByIndex(userId, 0).getDescription());
    }

    @Test
    public void changeProjectStatusByIndex() {
        service.create(userId, name, description);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeProjectStatusByIndex(null, 0, status));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.changeProjectStatusByIndex(userId, 1, status));
        Assert.assertThrows(StatusEmptyException.class, () -> service.changeProjectStatusByIndex(userId, 0, null));
        service.changeProjectStatusByIndex(userId, 0, status);
        Assert.assertEquals(service.findOneByIndex(userId, 0).getStatus(), status);
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final Project project = service.create(userId, name, description);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeProjectStatusById(null, project.getId(), status));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.changeProjectStatusById(userId, null, status));
        Assert.assertThrows(StatusEmptyException.class, () -> service.changeProjectStatusById(userId, project.getId(), null));
        service.changeProjectStatusById(userId, project.getId(), status);
        Assert.assertEquals(service.findOneById(userId, project.getId()).getStatus(), status);
    }


    @Test
    public void removeAll() {
        Assert.assertEquals(0, service.getSize());
        service.create(userId, UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Assert.assertEquals(1, service.getSize());
        service.create(userId, UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Assert.assertEquals(2, service.getSize());
        service.removeAll();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void existsById() {
        @NotNull final Project project = service.create(userId, name, description);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById(null, project.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, null));
        Assert.assertTrue(service.existsById(project.getId()));
    }

    @Test
    public void findAll() {
        repository.create(userId, name, description);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById(null,null));
        Assert.assertEquals(1, service.findAll().size());
        repository.create(userId, name, description);
        Assert.assertEquals(2, service.findAll().size());
        repository.create(userId, name, description);
        Assert.assertEquals(3, service.findAll().size());
        repository.create(userId, name, description);
        Assert.assertEquals(4, service.findAll().size());
    }

    @Test
    public void findAllByComparator() {
        final int projectsLength = 4;
        for (int i = 0; i < projectsLength; i++) {
            @NotNull final String name = String.format("Project_%d", projectsLength - i - 1);
            service.create(userId, name, description);
        }
        @NotNull List<Project> projects = service.findAll(userId, ProjectSort.BY_NAME.getComparator());
        Assert.assertEquals(projectsLength, projects.size());
        for (int i = 0; i < projectsLength; i++) {
            @NotNull final String name = String.format("Project_%d", i);
            Assert.assertEquals(name, projects.get(i).getName());
        }
    }

    @Test
    public void findOneById() {
        Assert.assertEquals(0, service.getSize());
        @NotNull final Project project = service.create(userId, name, description);
        Assert.assertNotNull(service.findOneById(project.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById(null, project.getId()));
        Assert.assertEquals(project.getId(), service.findOneById(userId, project.getId()).getId());
    }

    @Test
    public void findOneByIndex() {
        @NotNull final Project project = service.create(userId, name, description);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex(null,0));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(userId, 1));
        Assert.assertEquals(project.getId(), service.findOneByIndex(userId, 0).getId());
    }

    @Test
    public void removeOneById() {
        Assert.assertEquals(0, service.getSize());
        @NotNull final Project project = service.create(userId, name, description);
        Assert.assertEquals(1, service.getSize());
        Assert.assertThrows(IdEmptyException.class, () -> service.removeOneById(null));
        service.removeOneById(project.getId());
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void removeOneByIndex() {
        Assert.assertEquals(0, service.getSize());
        service.create(userId, name, description);
        Assert.assertEquals(1, service.getSize());
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeOneByIndex(1));
        service.removeOneByIndex(0);
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void add() {
        Assert.assertEquals(0, service.getSize());
        @NotNull final Project project = new Project();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(null, project));
        @Nullable Project newProject = service.add(userId, project);
        Assert.assertNotNull(newProject);
        Assert.assertEquals(project, newProject);
        Assert.assertEquals(1, service.getSize());
    }

    @Test
    public void removeOne() {
        final int repositorySize = 4;
        for (int i = 0; i < repositorySize; i++) {
            @NotNull final String nameRandom = UUID.randomUUID().toString();
            @NotNull final String descriptionRandom = UUID.randomUUID().toString();
            service.create(userId, nameRandom, descriptionRandom);
        }
        Assert.assertEquals(repositorySize, service.getSize(userId));
        @NotNull final Project project = service.removeOne(service.findOneByIndex(userId, 0));
        Assert.assertEquals(repositorySize - 1, service.getSize(userId));
        Assert.assertFalse(service.existsById(project.getId()));
    }

    @Test
    public void getSize() {
        final int repositorySize = 4;
        for (int i = 0; i < repositorySize; i++) {
            @NotNull final String userIdRandom = UUID.randomUUID().toString();
            @NotNull final String nameRandom = UUID.randomUUID().toString();
            @NotNull final String descriptionRandom = UUID.randomUUID().toString();
            service.create(userIdRandom, nameRandom, descriptionRandom);
        }
        @NotNull final List<Project> projects = service.findAll();
        Assert.assertEquals(repositorySize, projects.size());
    }

}
