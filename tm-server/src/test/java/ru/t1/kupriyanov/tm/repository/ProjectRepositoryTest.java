package ru.t1.kupriyanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.t1.kupriyanov.tm.api.repository.IProjectRepository;
import ru.t1.kupriyanov.tm.enumerated.ProjectSort;
import ru.t1.kupriyanov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String name = UUID.randomUUID().toString();

    @NotNull
    private final String description = UUID.randomUUID().toString();

    @Test
    public void create() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Project project = repository.create(userId, name, description);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), project.getId()));
        @Nullable final Project projectFind = repository.findOneById(userId, project.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project, projectFind);
        Assert.assertEquals(project.getDescription(), projectFind.getDescription());
    }

    @Test
    public void add() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        repository.add(project);
        Assert.assertEquals(1, repository.getSize());
    }

    @Test
    public void clear() {
        Assert.assertEquals(0, repository.getSize());
        repository.create(userId, name, description);
        Assert.assertEquals(1, repository.getSize());
        repository.removeAll();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void findAll() {
        repository.create(userId, name, description);
        Assert.assertEquals(1, repository.findAll().size());
        repository.create(userId, name, description);
        Assert.assertEquals(2, repository.findAll().size());
        repository.create(userId, name, description);
        Assert.assertEquals(3, repository.findAll().size());
        repository.create(userId, name, description);
        Assert.assertEquals(4, repository.findAll().size());
    }

    @Test
    public void findAllUserId() {
        repository.create(userId, name, description);
        @Nullable final String emptyUser = null;
        Assert.assertEquals(0, repository.findAll(UUID.randomUUID().toString()).size());
        Assert.assertEquals(1, repository.findAll(userId).size());
    }

    @Test
    public void findAllComparator() {
        final int countProject = 4;
        for (int i = 0; i < countProject; i++) {
            @NotNull final String name = String.format("Project_%d", countProject - i - 1);
            repository.create(userId, name, description);
        }
        @NotNull List<Project> projects = repository.findAll(userId, ProjectSort.BY_NAME.getComparator());
        Assert.assertEquals(countProject, projects.size());
        for (int i = 0; i < countProject; i++) {
            @NotNull final String projectName = String.format("Project_%d", i);
            Assert.assertEquals(projectName, projects.get(i).getName());
        }
    }

    @Test
    public void existsByIdUserCreator() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Project project = repository.create(userId, name, description);
        Assert.assertTrue(repository.existsById(project.getId()));
    }

    @Test
    public void existsByIdUserRandom() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Project project = repository.create(userId, name, description);
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString(), project.getId()));
    }

    @Test
    public void existsByIdUserEmpty() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Project project = repository.create(userId, name, description);
        Assert.assertFalse(repository.existsById("", project.getId()));
    }

    @Test
    public void findOneByIdUserCreator() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Project project = repository.create(userId, name, description);
        Assert.assertNotNull(repository.findOneById(project.getId()));
        @Nullable Project projectFind = repository.findOneById(userId, project.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project.getId(), projectFind.getId());
    }

    @Test
    public void findOneByIdUserRandom() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Project project = repository.create(userId, name, description);
        Assert.assertNotNull(repository.findOneById(project.getId()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), project.getId()));
        Assert.assertNull(repository.findOneById(userId, UUID.randomUUID().toString()));
    }

    @Test
    public void findOneByIdUserEmpty() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Project project = repository.create(userId, name, description);
        Assert.assertNotNull(repository.findOneById(project.getId()));
        Assert.assertNull(repository.findOneById(null, project.getId()));
        Assert.assertNull(repository.findOneById("", project.getId()));
    }

    @Test
    public void findOneByIndexUserCreator() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Project project = repository.create(userId, name, description);
        Assert.assertNotNull(repository.findOneByIndex(0));
        Assert.assertEquals(project.getId(), repository.findOneByIndex(0).getId());
        Assert.assertNotNull(repository.findOneByIndex(userId, 0));
    }

    @Test
    public void findOneByIndexUserRandom() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Project project = repository.create(userId, name, description);
        Assert.assertNotNull(repository.findOneByIndex(0));
        Assert.assertEquals(project.getId(), repository.findOneByIndex(0).getId());
        Assert.assertNull(repository.findOneByIndex(UUID.randomUUID().toString(), 0));
    }

    @Test
    public void findOneByIndexUserEmpty() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Project project = repository.create(userId, name, description);
        Assert.assertNotNull(repository.findOneByIndex(0));
        Assert.assertEquals(project.getId(), repository.findOneByIndex(0).getId());
        Assert.assertNull(repository.findOneByIndex("", 0));
    }

    @Test
    public void remove() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Project project = repository.create(userId, name, description);
        @Nullable final Project projectRemove = repository.removeOne(project);
        Assert.assertNotNull(projectRemove);
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void removeAll() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull List<Project> projects = new ArrayList<>();
        final int repositorySize = 4;
        for (int i = 0; i < repositorySize; i++) {
            @NotNull final String userId = UUID.randomUUID().toString();
            @NotNull final String projectName = UUID.randomUUID().toString();
            @NotNull final Project project = new Project();
            project.setName(projectName);
            project.setUserId(userId);
            projects.add(project);
        }
        repository.add(projects);
        Assert.assertEquals(repositorySize, repository.getSize());
        repository.removeAll();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void removeById() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Project project = repository.create(userId, name, description);
        Assert.assertEquals(1, repository.getSize());
        repository.removeOneById(UUID.randomUUID().toString());
        Assert.assertEquals(1, repository.getSize());
        repository.removeOneById(project.getId());
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void removeByIndex() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Project project = repository.create(userId, name, description);
        @Nullable final Project projectRemove = repository.removeOneByIndex(0);
        Assert.assertNotNull(projectRemove);
        Assert.assertEquals(0, repository.getSize());
        Assert.assertEquals(project, projectRemove);
    }

    @Test
    public void count() {
        Assert.assertEquals(0, repository.getSize());
        final int repositorySize = 4;
        for (int i = 0; i < repositorySize; i++) {
            @NotNull final String userId = UUID.randomUUID().toString();
            @NotNull final String name = UUID.randomUUID().toString();
            repository.create(userId, name, description);
            Assert.assertEquals(i + 1, repository.getSize());
            Assert.assertEquals(repository.getSize(), repository.findAll().size());
        }
    }

}
