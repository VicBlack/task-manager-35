package ru.t1.kupriyanov.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.kupriyanov.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}