package ru.t1.kupriyanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.kupriyanov.tm.api.service.IAuthService;
import ru.t1.kupriyanov.tm.api.service.IServiceLocator;
import ru.t1.kupriyanov.tm.dto.request.UserLoginRequest;
import ru.t1.kupriyanov.tm.dto.request.UserLogoutRequest;
import ru.t1.kupriyanov.tm.dto.request.UserProfileRequest;
import ru.t1.kupriyanov.tm.dto.response.UserLoginResponse;
import ru.t1.kupriyanov.tm.dto.response.UserLogoutResponse;
import ru.t1.kupriyanov.tm.dto.response.UserProfileResponse;
import ru.t1.kupriyanov.tm.model.Session;
import ru.t1.kupriyanov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface =  "ru.t1.kupriyanov.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    ) {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    ) {
        @NotNull final Session session = check(request);
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        authService.logout(session);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserProfileRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final User user = getServiceLocator().getUserService().findOneById(session.getUserId());
        return new UserProfileResponse(user);
    }

}
