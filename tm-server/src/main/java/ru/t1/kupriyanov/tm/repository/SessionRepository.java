package ru.t1.kupriyanov.tm.repository;

import ru.t1.kupriyanov.tm.api.repository.ISessionRepository;
import ru.t1.kupriyanov.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
