package ru.t1.kupriyanov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class UserUpdateProfileRequest extends AbstractUserRequest {

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    public UserUpdateProfileRequest(
            @Nullable String token,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    ) {
        super(token);
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
    }

}
