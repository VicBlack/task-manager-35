package ru.t1.kupriyanov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectGetByIdRequest extends AbstractIdRequest {

    public ProjectGetByIdRequest(@Nullable String token, @Nullable String id) {
        super(token, id);
    }

}
