package ru.t1.kupriyanov.tm.exception.entity;

public final class ModelNotFoundException extends AbstractEntityException {

    public ModelNotFoundException() {
        super("Error! Model wasn't found...");
    }

}
