package ru.t1.kupriyanov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.Socket;

public interface IEndpointClient {

    @Nullable
    Socket connect() throws IOException;

    @Nullable
    Socket disconnect() throws IOException;

    @Nullable
    Socket getSocket();

    void setSocket(@Nullable Socket socket);

}
