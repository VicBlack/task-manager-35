package ru.t1.kupriyanov.tm.exception;

import org.jetbrains.annotations.Nullable;

public class AbstractException extends RuntimeException {

    public AbstractException() {
    }

    public AbstractException(@Nullable final String message) {
        super(message);
    }

    public AbstractException(@Nullable final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }

    public AbstractException(@Nullable final Throwable cause) {
        super(cause);
    }

    public AbstractException(@Nullable final String message, @Nullable final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
