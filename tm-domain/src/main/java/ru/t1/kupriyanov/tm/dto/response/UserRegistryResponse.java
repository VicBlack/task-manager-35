package ru.t1.kupriyanov.tm.dto.response;

import org.jetbrains.annotations.NotNull;
import ru.t1.kupriyanov.tm.model.User;

public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@NotNull User user) {
        super(user);
    }

}
